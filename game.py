from random import randint

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    # TODO:
    # You need to delete the word "pass" and write your
    # function, here.
    pass


def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # TODO:
        # while the length of their response is not equal
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again

        # TODO:
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers

        # TODO:
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret

        # TODO:
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret

        # TODO:
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.

        # TODO:
        # report the number of "bulls" (exact matches)
        # TODO:
        # report the number of "cows" (common entries - exact matches)

    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.


# Runs the game
def run():
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    pass


if __name__ == "__main__":
    run()
